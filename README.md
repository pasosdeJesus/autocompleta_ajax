# autocompleta_ajax

Autocompletación para campos de texto que obtiene opciones de una
consulta con `Rails.Ajax`.

Se proveen dos formas de uso:

1. `AutocompletaAjaxCampotexto` que recibe un campo de texto y lo dota
    de autocompletación.
2. `AutocompletaAjaxExpreg` que dota de autocompletación a todos los campos
   de texto que cumplan una expresión regular (tanto los existentes en el DOM
   al cargar esta librería como los que se añadan posteriormente).

La aplicación que use este paquete también requiere `@rails/ujs`.


## Forma de uso en una aplicación rails

### Requisitos
* Su aplicación rails debe tener al menos un campo de texto que quiera dotar de autocompletación (digamos nombres de personas)
* Su aplicación rails debe tener un API JSON con una ruta que reciba el término de búsqueda (parámetro `term`) ingresado por el usuario y que responda en JSON con varias opciones de autocompletación  --es recomendable que responda máximo con 10 opciones.  Para ejemplificar la forma de uso suponderemos que tal ruta es `/personas.json`.

### Configuración
Agregue el paquete:

```sh
yarn add https://gitlab.com/pasosdeJesus/autocompleta_ajax.git
```

Carguelo desde `app/javascript/application.js` y ponga la clase principal en el objeto global:
```js
import {AutocompletaAjaxExpreg} from '@pasosdejesus/autocompleta_ajax'
window.AutocompletaAjaxExpreg = AutocompletaAjaxExpreg
```

En las vistas del campo de texto que se dotará de autocompletación agregue un elemento `datalist` con un `id` de su elección, por ejemplo:
```html
<datalist id='fuente-personas'>
</datalist>
```

Los campos que dotará de autocompletación deben tener el atributo `list=fuente-personas` por ejemplo si usa `simple-form` su ERB incluirá:
```erb
<%= f.input :nombre,
    input_html: { list: 'fuente-personas' } %>
```

En caso de usar `AutocompletaAjaxExpreg` debe haber una o varias expresiones regulares que sólo
las cumplan los `id` del o de los campos de texto que se dotará(n) de autocompletación.
Para este ejemplo supongamos que sirve esta expresión regular:
`/^caso_victima_attributes_[0-9]*_persona_attributes_nombres$/`

En el siguiente ejemplo supondremos que es la ruta `/personas.json` que responde a GET y puede recibir el término búscado en el parámetro `term` (tal como lo requeriría la autocompletación usando fuente AJAX de `jquery-ui`).

Inicie una clase como la sigiente en `app/javascript/AutocompletaAjaxPersona.js`
```js
export default class AutocompletaAjaxPersona {

    static idDatalist = 'fuente-personas'

  // Elije una persona en autocompletación
  static operarElegida (eorig, cadgrupo, id, otrosop) {

  }

  static iniciar() {
    console.log("AutocompletaAjaxPersona")
    let url = '/personas.json'
    var aePersonas = new window.AutocompletaAjaxExpreg(
      [ /^caso_victima_attributes_[0-9]*_persona_attributes_nombres$/]
      url,
      AutocompletaAjaxPersona.idDatalist,
      AutocompletaAjaxPersona.operarElegida
    )
    aePersonas.iniciar()
  }

}
```

Complete el método `operaElegida` que será llamado cuando el usuario elija una de las opciones
de autocompletacíon, por ejemplo para completar uno o más campos con información de la persona elegida.

Cada vez que se cargue en el navegador el javascript completo debe ejecutar el método `iniciar` de esa clase, pero sólo una vez, por ejemplo en `app/javascript/application`:

```js
AutocompletaAjaxPersona.iniciar()
```

