/**
 * Lógica de autocompletación empleando un datalist, extayendo opciones
 * con Ajax y llamando una función cuando el usuario elije una de las
 * posibles autocompletaciones.
 *
 * Inspirado en https://codepen.io/jrio/pen/dNKLMR por Joao Rodrigues
 * Solución para diferenciar cuando se teclea o cuando se elije basada en
 * la de Dan en https://stackoverflow.com/questions/30022728/perform-action-when-clicking-html5-datalist-option
 */
export default class AutocompletaAjaxBase {
  /** Constructra
   * @param url Responderá llamado Ajax con opciones e.g "/sip/personas.json"
   * @param idDatalist id de elemento datalist donde se pondrán opciones de
   *        autocompletación obtenidas con Ajax, e.g "fuente-asistentes"
   * @param operarElegida  función por llamar cuando se elija una opción
   *        su primer argumento es evento
   *        su segundo argumento es valor de opción elegida (o.value)
   *        su tercer argumento será texto de opción elegida (o.innerText)
   *        su cuarto argumento son otros datos asociados a la opción elegida
   */
  constructor (url, idDatalist, operarElegida) {
    this.url = url
    const dl = document.getElementById(idDatalist)
    this.idDatalist = idDatalist
    if (typeof operarElegida !== 'function') {
      window.alert('* AutocompletaAjax: operarElegida no es función')
      return false
    }
    this.operarElegida = operarElegida
    this.teclapresionada = false
    this.ultimoVal = ''
    this.ultimoTiempo = 0
    if (dl != null) {
      dl.innerHTML = ''
    }
  }

  /**
   * Añade opciones al datalist (con lo obtenido con AJAX)
   * @param {array} resp - arreglo de opciones para el datalist
   */
  llenarDatalist (resp) {
    // console.log('OJO llenarDatalist, resp=' + resp)
    const frag = document.createDocumentFragment()
    resp.forEach(item => {
      const opcion = document.createElement('option')
      for (const prop in item) {
        if (prop !== 'value' && prop !== 'id') {
          const nid = prop.replace(/\W/g, '-')
          const ip = item[prop] == null ? '' : item[prop]
          opcion.setAttribute('data-' + nid,
            prop.replace(/:/g, '|') + ':' + ip)
        }
      }
      // console.log(item)
      if (typeof (item.id) === 'string') {
        const pid = item.id
        // console.log(pid)
        const ppid = pid.split(';')
        // console.log(ppid)
        if (typeof (ppid[0]) === 'string') {
          [opcion.value, opcion.text] = [ppid[0], item.value]
          opcion.setAttribute('data-idorig', item.id)
        } else {
          window.alert('extraño problema')
        }
        frag.appendChild(opcion)
      } else if (typeof item.value === 'string') {
        if (typeof item.id === 'number') {
          [opcion.value, opcion.text] = [item.id, item.value]
        } else {
          [opcion.value, opcion.text] = [item.value, item.value]
        }
        frag.appendChild(opcion)
      }
    })
    const sel = document.getElementById(this.idDatalist)
    sel.innerHTML = ''
    if (frag.hasChildNodes()) {
      sel.appendChild(frag)
    }
  }

  /**
   * Usa texto tecleado por el usuario de más de 4 caracteres
   * para hacer llamada AJAX que busca con ese término y
   * después ejecuta llenarDatalist con el resultado
   * @param {event} e evento
   */
  buscar (e) {
    if (e.target.getAttribute('data-autocompleta') === 'no') {
      // console.log('OJO ya no autocompleta')
      return
    }
    const val = e.target.value
    // keyup se lanza varias veces consecutivas  evitamos reiterar
    // llamada AJAX con el mismo valor en menos de 2 segundos
    this.tiempo = Date.now()
    const d = this.tiempo - this.ultimoTiempo
    if (this.ultimoVal === val && d > 0 && d < 2) {
      return
    }
    this.ultimoVal = val
    this.ultimoTiempo = this.tiempo
    // console.log('OJO buscar, val=' + val)
    if (val.length < 4) {
      return
    }
    let p = new URLSearchParams()
    let izq = this.url
    const posi = this.url.indexOf('?')
    if (posi >= 0) {
      p = new URLSearchParams(this.url.substr(posi + 1))
      izq = this.url.substr(0, posi)
    }
    p.append('term', val)
    const u = izq + '?' + p.toString()
    window.Rails.ajax({
      type: 'GET',
      url: u,
      data: null,
      success: (resp, estado, xhr) => {
        if (e.target.getAttribute('data-autocompleta') === 'no') {
          // Se agrega este if por si entra aquí después de haber
          // elegido uno de los autocompletados
          // console.log('OJO ya no autocompleta')
        } else {
          this.llenarDatalist(resp)
        }
      },
      error: (req, estado, xhr) => {
        window.alert('No pudo consultarse listado de personas.')
      }
    })
  };

  /**
   * Detecta teclas presionadas para diferenciar algo tecleado de click en
   * opción
   * @param {event} e evento
   */
  manejarEventoKeydown (e) {
    // console.log('OJO keydown ' + e.target.id)
    if (e.key) {
      this.teclapresionada = true
    }
  }

  /**
   * Detecta teclas levantadas para mejorar listado de datalist
   * @param {event} e evento
   */
  manejarEventoKeyup (e) {
    // console.log('OJO keyup ' + e.target.id)
    this.buscar(e)
  }

  /**
   * Detecta cuando se elige una de las opciones autocompletadas.
   * @param {event} e evento
   */
  manejarEventoInput (e) {
    // console.log('OJO input ' + e.target.id)
    if (this.teclapresionada === false) {
      /* Aquí está el problema con datalist porque no tenemos forma
       * de saber el id de la opción elegida, e.target.value tiene el
       * texto, pero podría haber varias entradas con el mismo texto.
       * Lo menos peor que se puede hacer es buscar entra las opciones
       * la primera cuyo texto coincida y elegir esa
       */
      const el = document.querySelector('#' + this.idDatalist)
      let idop = null
      let nomop = null
      const otrosop = {}
      el.childNodes.forEach((o) => {
        if (o.value === e.target.value) {
          idop = o.value
          nomop = o.innerText
          const idorigop = o.getAttribute('data-idorig')
          otrosop['data-idorig'] = idorigop
          for (const [llave, valor] of Object.entries(o.dataset)) {
            const ud = valor.indexOf(':')
            if (llave !== 'value' && ud >= 0) {
              const nomprop = valor.substr(0, ud)
              const valprop = valor.substr(ud + 1)
              otrosop[nomprop] = valprop
            }
          }
        }
      })

      if (idop != null) {
        e.stopPropagation() // No hace burbuja
        e.preventDefault() // No ejecuta acción predeterminada
        e.target.value = '' // Borrar la id del elemento elegido
        this.operarElegida(e, nomop, idop, otrosop)
      } else {
        // window.alert('Problema para identificar opción elegida')
      }
    }
    this.teclapresionada = false
  }
}
