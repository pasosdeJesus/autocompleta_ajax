import AutocompletaAjaxBase from './AutocompletaAjaxBase'

// Suponemos que application.js ya hizo:
// import Rails from "@rails/ujs"
// windows.Rails=Rails

/**
 * Establece un control de autocompletación en campos cuyo id cumpla
 * una expresión regular
 */
export default class AutocompletaAjaxExpreg extends AutocompletaAjaxBase {
  /** Constructra
   * @param expregselem Vector con expresiones regular de identificadores de campos por dotar de autocompletación e.g [/^actividad_asistencia_attributes_[0-9]*_persona_attributes_numerodocumento$/]
   * @param url "/sip/personas.json"
   * @param idDatalist "fuente-asistentes"
   * @param operarElegida  función por llamar cuando se elija una opción debe tener argumentos evento, nomop, idop, otrosop
   */
  constructor (expregselem, url, idDatalist, operarElegida) {
    super(url, idDatalist, operarElegida)
    this.expregselem = expregselem
  }

  funKeyup (e) {
  }

  iniciar () {
    /**
     * Detecta teclas levantadas para mejorar listado de datalist
     * @param {event} e evento
     */
    console.log('Agrega escuchadores de eventos para datalist con id', this.idDatalist)
    document.addEventListener('keyup', (e) => {
      this.expregselem.forEach((ere) => {
        if (ere.test(e.target.id)) {
          this.manejarEventoKeyup(e)
        }
      })
    })

    /**
     * Detecta teclas presionadas para diferenciar algo tecleado de click en opción
     * @param {event} e evento
     */
    if (document.body.getAttribute('data-autocompleta-ajax-expreg-keydown-' +
      this.idDatalist) === null) {
      document.body.setAttribute('data-autocompleta-ajax-expreg-keydown-' + this.idDatalist,
        'definido')
      document.addEventListener('keydown', (e) => {
        this.expregselem.forEach((ere) => {
          if (ere.test(e.target.id)) {
            this.manejarEventoKeydown(e)
          }
        })
      }, false)
    }

    /**
     * Detecta cuando se elige una de las opciones autocompletadas.
     * @param {event} e evento
     */
    if (document.body.getAttribute('data-autocompleta-ajax-expreg-input-' +
      this.idDatalist) === null) {
      document.body.setAttribute('data-autocompleta-ajax-expreg-input-' + this.idDatalist,
        'definido')
      document.addEventListener('input', (e) => {
        this.expregselem.forEach((ere) => {
          if (ere.test(e.target.id)) {
            this.manejarEventoInput(e)
          }
        })
      }, false)
    }
  }
}
