import AutocompletaAjaxBase from './AutocompletaAjaxBase'

// Suponemos que application.js ya hizo:
// import Rails from "@rails/ujs"
// windows.Rails=Rails

/**
 * Establece un control de autocompletación en un campo de texto
 */
export default class AutocompletaAjaxCampotexto extends AutocompletaAjaxBase {
  /** Constructora
   * @param campotexto document.querySelector('#micampo1')
   * @param url "/sip/personas.json"
   * @param idDatalist "fuente-asistentes"
   * @param operarElegida  función por llamar cuando se elija una opción debe tener argumentos e,nomop, idop, otrosop
   */
  constructor (campotexto, url, idDatalist, operarElegida) {
    super(url, idDatalist, operarElegida)
    this.campotexto = campotexto
    campotexto.setAttribute('data-autocompleta-ajax', '1')
  }

  iniciar () {
    this.campotexto.addEventListener('keyup', (e) => {
      this.manejarEventoKeyup(e)
    }, false)

    this.campotexto.addEventListener('keydown', (e) => {
      this.manejarEventoKeydown(e)
    }, false)

    this.campotexto.addEventListener('input', (e) => {
      this.manejarEventoInput(e)
    }, false)
  }
}
