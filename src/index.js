import AutocompletaAjaxCampotexto from './AutocompletaAjaxCampotexto'
import AutocompletaAjaxExpreg from './AutocompletaAjaxExpreg'

export {
  AutocompletaAjaxCampotexto,
  AutocompletaAjaxExpreg
}
